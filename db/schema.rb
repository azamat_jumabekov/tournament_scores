# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_06_072651) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "group_matches_lists", force: :cascade do |t|
    t.bigint "tournament_id", null: false
    t.bigint "match_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["match_id"], name: "index_group_matches_lists_on_match_id"
    t.index ["tournament_id"], name: "index_group_matches_lists_on_tournament_id"
  end

  create_table "matches", force: :cascade do |t|
    t.bigint "team_a_id"
    t.bigint "team_b_id"
    t.string "team_a_score"
    t.string "team_b_score"
    t.bigint "winner_team_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["team_a_id"], name: "index_matches_on_team_a_id"
    t.index ["team_b_id"], name: "index_matches_on_team_b_id"
    t.index ["winner_team_id"], name: "index_matches_on_winner_team_id"
  end

  create_table "playoff_matches_lists", force: :cascade do |t|
    t.bigint "tournament_id", null: false
    t.bigint "match_id", null: false
    t.integer "stage"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["match_id"], name: "index_playoff_matches_lists_on_match_id"
    t.index ["tournament_id"], name: "index_playoff_matches_lists_on_tournament_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "tournament_teams", force: :cascade do |t|
    t.bigint "tournament_id"
    t.bigint "team_id"
    t.integer "division"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["team_id"], name: "index_tournament_teams_on_team_id"
    t.index ["tournament_id"], name: "index_tournament_teams_on_tournament_id"
  end

  create_table "tournaments", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "group_matches_lists", "matches"
  add_foreign_key "group_matches_lists", "tournaments"
  add_foreign_key "matches", "teams", column: "team_a_id"
  add_foreign_key "matches", "teams", column: "team_b_id"
  add_foreign_key "matches", "teams", column: "winner_team_id"
  add_foreign_key "playoff_matches_lists", "matches"
  add_foreign_key "playoff_matches_lists", "tournaments"
end
