# frozen_string_literal: true

class CreateGroupMatchesLists < ActiveRecord::Migration[6.0]
  def change
    create_table :group_matches_lists do |t|
      t.references :tournament, null: false, foreign_key: true
      t.references :match, null: false, foreign_key: true

      t.timestamps
    end
  end
end
