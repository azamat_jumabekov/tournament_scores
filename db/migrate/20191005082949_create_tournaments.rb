# frozen_string_literal: true

class CreateTournaments < ActiveRecord::Migration[6.0]
  def change
    create_table :tournaments do |t|
      t.string :title

      t.timestamps
    end
  end
end
