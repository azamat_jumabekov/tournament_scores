# frozen_string_literal: true

class CreateTournamentTeams < ActiveRecord::Migration[6.0]
  def change
    create_table :tournament_teams do |t|
      t.belongs_to :tournament
      t.belongs_to :team
      t.integer :division

      t.timestamps
    end
  end
end
