# frozen_string_literal: true

class CreatePlayoffMatchesLists < ActiveRecord::Migration[6.0]
  def change
    create_table :playoff_matches_lists do |t|
      t.references :tournament, null: false, foreign_key: true
      t.references :match, null: false, foreign_key: true
      t.integer :stage

      t.timestamps
    end
  end
end
