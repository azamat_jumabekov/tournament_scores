# frozen_string_literal: true

class CreateMatches < ActiveRecord::Migration[6.0]
  def change
    create_table :matches do |t|
      t.belongs_to :team_a, foreign_key: { to_table: :teams }, index: true
      t.belongs_to :team_b, foreign_key: { to_table: :teams }, index: true
      t.string :team_a_score
      t.string :team_b_score
      t.references :winner_team, foreign_key: { to_table: :teams }, index: true

      t.timestamps
    end
  end
end
