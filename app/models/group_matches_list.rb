# frozen_string_literal: true

class GroupMatchesList < ApplicationRecord
  belongs_to :tournament
  belongs_to :group_match, class_name: 'Match', foreign_key: :match_id
end
