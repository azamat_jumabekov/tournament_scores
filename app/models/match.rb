# frozen_string_literal: true

class Match < ApplicationRecord
  belongs_to :team_a, class_name: 'Team'
  belongs_to :team_b, class_name: 'Team'

  before_update :set_winner

  default_scope { includes(:team_a, :team_b) }

  private

  def set_winner
    if team_a_score.to_i > team_b_score.to_i
      self.winner_team_id = team_a_id
    else
      self.winner_team_id = team_b_id
    end
  end
end
