# frozen_string_literal: true

class PlayoffMatchesList < ApplicationRecord
  STAGES = {
    finals: 0,
    semifinals: 1,
    quarterfinals: 2
  }.freeze

  belongs_to :tournament
  belongs_to :playoff_match, class_name: 'Match', foreign_key: :match_id

  enum stage: STAGES

  scope :quarterfinals_matches, -> { where(stage: 'quarterfinals') }
  scope :semifinals_matches, -> { where(stage: 'semifinals') }
  scope :finals_matches, -> { where(stage: 'finals') }
end
