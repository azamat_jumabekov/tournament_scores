# frozen_string_literal: true

class TournamentTeam < ApplicationRecord
  belongs_to :tournament
  belongs_to :team

  scope :division1, -> { where(division: 1) }
  scope :division2, -> { where(division: 2) }
end
