# frozen_string_literal: true

class Tournament < ApplicationRecord
  has_many :group_matches_lists, dependent: :destroy
  has_many :group_matches, through: :group_matches_lists

  has_many :playoff_matches_lists, dependent: :destroy
  has_many :playoff_matches, through: :playoff_matches_lists

  has_many :tournament_teams, dependent: :destroy
  has_many :teams, through: :tournament_teams

  def quarterfinals_matches
    playoff_matches.merge(PlayoffMatchesList.quarterfinals_matches)
  end

  def semifinals_matches
    playoff_matches.merge(PlayoffMatchesList.semifinals_matches)
  end

  def finals_matches
    playoff_matches.merge(PlayoffMatchesList.finals_matches)
  end

  def division1_teams
    teams.merge(TournamentTeam.division1)
  end

  def division2_teams
    teams.merge(TournamentTeam.division2)
  end

  def division1_matches
    ids = division1_teams.pluck(:id)
    group_matches.where(team_a_id: ids).or(group_matches.where(team_b_id: ids))
  end

  def division2_matches
    ids = division2_teams.pluck(:id)
    group_matches.where(team_a_id: ids).or(group_matches.where(team_b_id: ids))
  end

  def playoff_teams
    teams.merge(TournamentTeam.division1).merge(TournamentTeam.division2)
  end
end
