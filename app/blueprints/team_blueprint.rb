# frozen_string_literal: true

class TeamBlueprint < Blueprinter::Base
  identifier :id
  field :title
end
