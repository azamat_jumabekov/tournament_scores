# frozen_string_literal: true

class TournamentBlueprint < Blueprinter::Base
  identifier :id
  fields :title

  view :all_included do
    association :group_matches, blueprint: MatchBlueprint
    association :division1_matches, blueprint: MatchBlueprint
    association :division2_matches, blueprint: MatchBlueprint
    association :division1_teams, blueprint: TeamBlueprint
    association :division2_teams, blueprint: TeamBlueprint
    association :quarterfinals_matches, blueprint: MatchBlueprint
    association :semifinals_matches, blueprint: MatchBlueprint
    association :finals_matches, blueprint: MatchBlueprint
  end
end
