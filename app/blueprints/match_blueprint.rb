# frozen_string_literal: true

class MatchBlueprint < Blueprinter::Base
  identifier :id
  fields :team_a_score, :team_b_score, :winner_team_id

  association :team_a, blueprint: TeamBlueprint
  association :team_b, blueprint: TeamBlueprint
end
