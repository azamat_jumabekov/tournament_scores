# frozen_string_literal: true

module TournamentResults
  class MatchesScoresFiller
    def initialize(tournament, stage)
      @tournament = tournament
      @stage = stage
      @matches = select_matches_stage
    end

    def call
      @matches.each do |group_match|
        scores = random_scores(group_match)
        group_match.update(scores)
      end
    end

    private

    def select_matches_stage
      case @stage
      when 'group'
        @tournament.group_matches
      when 'quarterfinals'
        @tournament.quarterfinals_matches
      when 'semifinals'
        @tournament.semifinals_matches
      when 'finals'
        @tournament.finals_matches
      else
        []
      end
    end

    def random_scores(group_match)
      loop do
        team_a_score = rand(0..2)
        team_b_score = rand(0..2)
        if team_a_score == team_b_score
          next
        else
          winner_id = (team_a_score > team_b_score) ? group_match.team_a_id : group_match.team_b_id
          return {
            team_a_score: team_a_score,
            team_b_score: team_b_score,
            winner_team_id: winner_id
          }
        end
      end
    end
  end
end
