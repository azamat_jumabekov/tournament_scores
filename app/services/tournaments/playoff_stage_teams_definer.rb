# frozen_string_literal: true

module Tournaments
  class PlayoffStageTeamsDefiner
    def initialize(tournament, stage)
      @tournament = tournament
      @stage = stage
      @matches = select_matches_by_stage
    end

    def call
      return if next_stage_matches.count == next_stage[:matches_count]
      create_next_stage_matches
    end

    private

    def create_next_stage_matches
      winner_teams_ids.each_slice(2) do |team_a_id, team_b_id|
        semifinals = Match.create(team_a_id: team_a_id, team_b_id: team_b_id)
        PlayoffMatchesList.create(tournament: @tournament, match_id: semifinals.id, stage: next_stage[:title])
      end
    end

    def winner_teams_ids
      @winner_teams_ids ||= @matches.pluck(:winner_team_id)
    end

    def select_matches_by_stage
      case @stage
      when 'quarterfinals'
        @tournament.quarterfinals_matches
      when 'semifinals'
        @tournament.semifinals_matches
      when 'finals'
        @tournament.finals_matches
      else
        []
      end
    end

    def next_stage
      {
        'quarterfinals' => { title: 'semifinals', matches_count: 2 },
        'semifinals' => { title: 'finals', matches_count: 1 }
      }.fetch(@stage)
    end

    def next_stage_matches
      case @stage
      when 'quarterfinals'
        @tournament.semifinals_matches
      when 'semifinals'
        @tournament.finals_matches
      end
    end
  end
end
