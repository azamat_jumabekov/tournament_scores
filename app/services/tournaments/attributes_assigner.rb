# frozen_string_literal: true

module Tournaments
  class AttributesAssigner
    def initialize(tournament, tournament_params, teams_params)
      @teams_params = teams_params
      @tournament_params = tournament_params
      @tournament = tournament
    end

    def call
      build_tournament
      build_tournament_teams
      @tournament
    end

    def build_tournament
      @tournament.assign_attributes(@tournament_params)
    end

    def build_tournament_teams
      @teams_params.each do |team_params|
        if (team_id = team_params.delete(:id))
          @tournament.teams.find(team_id).update(team_params)
        else
          @tournament.teams.build(team_params)
        end
      end
    end
  end
end


