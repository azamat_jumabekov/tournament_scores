# frozen_string_literal: true

module Tournaments
  class Handler
    attr_accessor :tournament

    def initialize(params, tournament = nil)
      @teams_params = params.delete(:teams)
      @tournament_params = params.slice(:title)
      @tournament = tournament || Tournament.new
    end

    def call
      validate_params
      assign_attributes
      tournament.save
      split_teams_into_divisions
      tournament
    end

    private

    def split_teams_into_divisions
      ids = tournament.tournament_teams.pluck(:id).shuffle
      TournamentTeam.where(id: ids[0..7]).update_all(division: 1)
      TournamentTeam.where(id: ids[8..16]).update_all(division: 2)
    end

    def validate_params
      return if @tournament.persisted?
      return if @teams_params.count == 16

      raise Tournaments::Errors::TeamsCountNotAcceptable, '16 teams allowed'
    end

    def assign_attributes
      Tournaments::AttributesAssigner.new(@tournament, @tournament_params, @teams_params).call
    end
  end
end
