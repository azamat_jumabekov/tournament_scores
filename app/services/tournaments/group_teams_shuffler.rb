# frozen_string_literal: true

module Tournaments
  class GroupTeamsShuffler
    def initialize(tournament)
      @tournament = tournament
      @division1_teams_ids = tournament.division1_teams.pluck(:id)
      @division2_teams_ids = tournament.division2_teams.pluck(:id)
    end

    def call
      create_matches_for_each_division
    end

    private

    def create_matches_for_each_division
      [@division1_teams_ids, @division2_teams_ids].each do |division|
        matched_teams_ids = shuffle_teams(division)
        matched_teams_ids.each do |team_a, team_b|
          @tournament.group_matches.create(
            team_a_id: team_a,
            team_b_id: team_b
          )
        end
      end
    end

    def shuffle_teams(division)
      [].tap do |games|
        division.each do |team|
          division.reject { |t| t == team }.each do |remaining_team|
            game = [team, remaining_team].sort
            games.push(game) unless games.include?(game)
          end
        end
      end
    end
  end
end
