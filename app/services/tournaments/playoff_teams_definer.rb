# frozen_string_literal: true

module Tournaments
  class PlayoffTeamsDefiner
    def initialize(tournament)
      @tournament = tournament
      @division1_matches = tournament.division1_matches
      @division2_matches = tournament.division2_matches
    end

    def call
      return if @tournament.quarterfinals_matches.count == 4
      division1_results = calculate_each_team_total_scores(@division1_matches)
      division2_results = calculate_each_team_total_scores(@division2_matches)
      playoff_teams = [*division1_results[4..7], *division2_results[4..7]]
      create_payoff_matches(playoff_teams)
    end

    private

    def calculate_each_team_total_scores(matches)
      totals = {}
      matches.each do |group_match|
        totals[group_match.team_a_id] ||= 0
        totals[group_match.team_a_id] += group_match.team_a_score.to_i

        totals[group_match.team_b_id] ||= 0
        totals[group_match.team_b_id] += group_match.team_b_score.to_i
      end
      totals.to_a.sort_by { |t| t[1] }
    end

    def create_payoff_matches(teams)
      raise 'Teams count must be 8' if teams.count != 8

      playoff_pairs = []

      while teams.size >= 2 do
        playoff_pairs << [teams.pop, teams.shift]
      end

      ids = []
      playoff_pairs.each do |pair|
        quarter_final_match = @tournament.playoff_matches.create(
          team_a_id: pair[0][0],
          team_b_id: pair[1][0]
        )
        ids << quarter_final_match.id
      end
      PlayoffMatchesList.where(match_id: ids).update_all(stage: 'quarterfinals')
    end
  end
end
