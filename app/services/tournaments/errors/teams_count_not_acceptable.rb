# frozen_string_literal: true

module Tournaments
  module Errors
    class TeamsCountNotAcceptable < StandardError
    end
  end
end
