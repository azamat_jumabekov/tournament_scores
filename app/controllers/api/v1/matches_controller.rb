# frozen_string_literal: true

module Api
  module V1
    class MatchesController < BaseController
      before_action :find_match, only: [:update]

      def update
        if @match.update(sanitized_params)
          head 204
        else
          head 422
        end
      end

      private

      def find_match
        @match = Match.find(params[:id])
      end

      def sanitized_params
        params.require(:match).permit(:team_a_score, :team_b_score)
      end
    end
  end
end
