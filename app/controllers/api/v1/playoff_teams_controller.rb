# frozen_string_literal: true

module Api
  module V1
    class PlayoffTeamsController < BaseController
      before_action :find_tournament, only: %i[create next_stage]

      def create
        if Tournaments::PlayoffTeamsDefiner.new(@tournament).call
          head 201
        else
          head 422
        end
      end

      def next_stage
        if Tournaments::PlayoffStageTeamsDefiner.new(@tournament, params['stage']).call
          head 201
        else
          head 422
        end
      end

      private

      def find_tournament
        @tournament = Tournament.find(params[:tournament_id])
      end
    end
  end
end
