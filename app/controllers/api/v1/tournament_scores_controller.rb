# frozen_string_literal: true

module Api
  module V1
    class TournamentScoresController < BaseController
      before_action :find_tournament, only: %i[fill_scores fill_all]

      def fill_scores
        if TournamentResults::MatchesScoresFiller.new(@tournament, params['stage']).call
          head 201
        else
          head 422
        end
      end

      def fill_all
        TournamentResults::MatchesScoresFiller.new(@tournament, 'group').call
        Tournaments::PlayoffTeamsDefiner.new(@tournament).call
        TournamentResults::MatchesScoresFiller.new(@tournament, 'quarterfinals').call
        Tournaments::PlayoffStageTeamsDefiner.new(@tournament, 'quarterfinals').call
        TournamentResults::MatchesScoresFiller.new(@tournament, 'semifinals').call
        Tournaments::PlayoffStageTeamsDefiner.new(@tournament, 'semifinals').call
        TournamentResults::MatchesScoresFiller.new(@tournament, 'finals').call

        head 201
      end

      private

      def find_tournament
        @tournament = Tournament.find(params[:tournament_id])
      end
    end
  end
end
