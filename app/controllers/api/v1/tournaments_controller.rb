# frozen_string_literal: true

module Api
  module V1
    class TournamentsController < BaseController
      before_action :find_tournament, only: %i[update destroy show]

      def index
        render json: Tournament.all
      end

      def create
        tournament = Tournaments::Handler.new(sanitized_params).call
        if tournament.errors.present?
          render json: { errors: tournament.errors.full_messages }
        else
          Tournaments::GroupTeamsShuffler.new(tournament).call
          render json: TournamentBlueprint.render(tournament, view: :all_included), status: :created
        end

      rescue Tournaments::Errors::TeamsCountNotAcceptable => e
        render json: { error: e.message }, status: 422
      end

      def show
        render json: TournamentBlueprint.render(@tournament, view: :all_included)
      end

      def destroy
        if @tournament.destroy
          head 204
        else
          render json: { errors: @tournament.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def update
        tournament = Tournaments::Handler.new(sanitized_params, @tournament).call
        if tournament.errors.present?
          render json: { errors: tournament.errors.full_messages }
        else
          render json: tournament.as_json, status: :ok
        end
      end

      private

      def find_tournament
        @tournament = Tournament.find(params[:id])
      end

      def sanitized_params
        params.require(:tournament).permit(:id, :title, teams: %i[id title])
      end
    end
  end
end
