# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'home#index'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :tournaments
      resources :matches, only: [:update]
      resources :playoff_teams, only: [:create] do
        collection do
          post :next_stage, to: 'playoff_teams/next_stage'
        end
      end
      namespace :tournament_scores do
        post :fill_scores, to: 'tournament_scores/fill_scores'
        post :fill_all, to: 'tournament_scores/fill_all'
      end
    end
  end
end
