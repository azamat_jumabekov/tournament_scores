# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Match, type: :model do
  describe 'create' do
    let(:subject) { create(:match) }

    it 'succeeds' do
      expect(subject.team_a).to be_kind_of(Team)
      expect(subject.team_b).to be_kind_of(Team)
    end
  end
end
