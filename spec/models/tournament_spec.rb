# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Tournament, type: :model do
  let(:subject) { create(:tournament, :with_finals) }

  describe 'create' do
    it 'succeeds' do
      expect(subject.group_matches_lists.count).to eq(56)
      expect(subject.playoff_matches_lists.count).to eq(7)
      expect(subject.quarterfinals_matches.count).to eq(4)
      expect(subject.semifinals_matches.count).to eq(2)
      expect(subject.finals_matches.count).to eq(1)
    end
  end
end
