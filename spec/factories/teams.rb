# frozen_string_literal: true

FactoryBot.define do
  factory :team do
    sequence :title do |n|
      "Team #{n}"
    end
  end
end
