# frozen_string_literal: true

FactoryBot.define do
  factory :group_matches_list do
    association :tournament, factory: :tournament
    association :group_match, factory: :match
  end
end
