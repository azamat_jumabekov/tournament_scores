# frozen_string_literal: true

FactoryBot.define do
  factory :tournament do
    sequence :title do |n|
      "World Cup #{2010 + n}"
    end

    trait :with_teams do
      after(:create) do |tournament|
        create_list(:tournament_team, 8, tournament: tournament, division: 1)
        create_list(:tournament_team, 8, tournament: tournament, division: 2)
      end
    end

    trait :with_group do
      with_teams
      after(:create) do |tournament|
        Tournaments::GroupTeamsShuffler.new(tournament).call
        TournamentResults::MatchesScoresFiller.new(tournament, 'group').call
      end
    end

    trait :with_quarterfinals do
      with_group
      after(:create) do |tournament|
        Tournaments::PlayoffTeamsDefiner.new(tournament).call
        TournamentResults::MatchesScoresFiller.new(tournament, 'quarterfinals').call
      end
    end

    trait :with_semifinals do
      with_quarterfinals
      after(:create) do |tournament|
        Tournaments::PlayoffStageTeamsDefiner.new(tournament, 'quarterfinals').call
        TournamentResults::MatchesScoresFiller.new(tournament, 'semifinals').call
      end
    end

    trait :with_finals do
      with_semifinals
      after(:create) do |tournament|
        Tournaments::PlayoffStageTeamsDefiner.new(tournament, 'semifinals').call
        TournamentResults::MatchesScoresFiller.new(tournament, 'finals').call
      end
    end
  end
end
