# frozen_string_literal: true

FactoryBot.define do
  factory :tournament_team do
    association :tournament, factory: :tournament
    association :team, factory: :team
  end
end
