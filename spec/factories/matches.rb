# frozen_string_literal: true

FactoryBot.define do
  factory :match do
    team_a_score { [*0..2].sample }
    team_b_score { [*0..2].sample }

    association :team_a, factory: :team
    association :team_b, factory: :team

    trait :with_winner do
      association :winner_team, factory: :team
    end
  end
end
