# frozen_string_literal: true

FactoryBot.define do
  factory :playoff_matches_list do
    association :tournament, factory: :tournament
    association :playoff_match, factory: :match
  end
end
