# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Tournaments::PlayoffTeamsDefiner do
  let(:tournament) { create(:tournament, :with_group) }
  let(:subject) { described_class.new(tournament) }

  before do
    subject.call
  end

  it 'creates LockedUser record' do
    expect(tournament.reload.playoff_matches.count).to eq(4)
  end
end
