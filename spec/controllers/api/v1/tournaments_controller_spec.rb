# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'TournamentsController', type: :request do
  describe 'GET index' do
    let!(:tournament) { create_list(:tournament, 2) }

    it 'succeeds' do
      get '/api/v1/tournaments'

      expect(response_json.map { |r| r['id'] }).to eq(Tournament.pluck(:id))
    end
  end

  describe 'GET show' do
    let(:tournament) { create(:tournament, :with_finals) }

    it 'returns tournament' do
      get '/api/v1/tournaments/' + tournament.id.to_s

      expect(response_json['id']).to eq(tournament.id)
    end
  end

  describe 'POST create' do
    let(:teams_count) { 16 }
    let(:teams_params) { teams_count.times.map { attributes_for(:team) } }
    let(:params) { { **attributes_for(:tournament), teams: teams_params } }
    let(:tournament) { Tournament.last }

    it 'creates tournament' do
      post '/api/v1/tournaments', params: { tournament: params }

      expect(response.code).to eq('201')
      expect(tournament.title).to eq(params[:title])
      expect(tournament.teams.count).to eq(teams_count)
      expect(tournament.division2_teams.count).to eq(8)
      expect(tournament.group_matches.count).to eq(56)
      expect(tournament.division1_matches.count).to eq(28)
      expect(tournament.division2_matches.count).to eq(28)
    end
  end

  describe 'PUT update' do
    let(:tournament) { create(:tournament, :with_teams) }
    let(:teams_params) do
      tournament.teams.map do |team|
        attributes_for(:team, id: team.id)
      end
    end
    let(:params) do
      {
        **attributes_for(:tournament, id: tournament.id),
        teams: teams_params
      }
    end

    it 'updates tournament' do
      put '/api/v1/tournaments/' + tournament.id.to_s, params: { tournament: params }

      expect(response.code).to eq('200')
      tournament.reload
      expect(tournament.title).to eq(params[:title])
      expect(tournament.reload.teams[0].title).to eq(teams_params[0][:title])
      expect(tournament.reload.teams[1].title).to eq(teams_params[1][:title])
    end
  end

  describe 'DELETE destroy' do
    let(:tournament) { create(:tournament) }

    it 'deletes tournament' do
      delete '/api/v1/tournaments/' + tournament.id.to_s

      expect(response).to be_successful
      expect(Tournament.pluck(:id)).not_to include(tournament.id)
    end
  end
end
