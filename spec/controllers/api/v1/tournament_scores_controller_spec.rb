# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'TournamentsController', type: :request do
  describe 'PUT fill_scores' do
    %w[group quarterfinals semifinals finals].each do |stage|
      describe "fills match results for #{stage}" do
        let(:factory_name) do
          trait = "with_#{stage}".to_sym
          [:tournament, trait]
        end
        let(:tournament) { create(*factory_name) }
        let(:params) { { tournament_id: tournament.id, stage: stage } }
        let(:matches) { "#{stage}_matches".to_sym }

        it 'succeeds' do
          post '/api/v1/tournament_scores/fill_scores/', params: params

          expect(tournament.send(matches).where(winner_team_id: nil).count).to be_zero
          expect(tournament.send(matches)).to be_present
        end
      end
    end
  end
end
